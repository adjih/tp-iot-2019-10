#! /bin/sh
#---------------------------------------------------------------------------
# Cedric Adjih - 2019
#---------------------------------------------------------------------------

grep -q tp-iot-2019-10 ~/.bashrc || {
    printf "+ Adding tp-iot-2019-10/tools in PATH setting in .bashrc\n"
    cp -a ~/.bashrc ~/.bashrc-back || exit 1
    printf '\nexport PATH=$HOME/tp-iot-2019-10/tools:$PATH\n' >> ~/.bashrc
}

#---------------------------------------------------------------------------

sudo apt install -y ash
#sudo apt update
#sudo apt install -y screen
# sudo apt-get -y install gcc-msp430 msp430-libc

#---------------------------------------------------------------------------
